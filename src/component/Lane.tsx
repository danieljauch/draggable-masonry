import React from "react"
import { View, ViewStyle } from "react-native"

import Tile from "./Tile"

interface Props {
	laneStyle: ViewStyle
	tiles: Array<TileType>
}

export default function Lane({ laneStyle, tiles }: Props) {
	return <View style={laneStyle}>{tiles.map((tile) => <Tile {...tile} />)}</View>
}
