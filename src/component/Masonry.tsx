import React from "react"
import { View, ViewStyle } from "react-native"

import Lane from "./Lane"
import { TileType } from "./src/lib/interfaces"

interface Props {
	lanes: number
	stylesheets: {
		containerStyle: ViewStyle
		laneStyle: ViewStyle
	}
	tiles: Array<TileType>
}

export default function Masonry({ lanes, stylesheets: { containerStyle, laneStyle }, tiles }: Props) {
	return <View style={containerStyle}>{Array(lanes).map((lane) => <Lane laneStyle={laneStyle} tiles={tiles} />)}</View>
}
