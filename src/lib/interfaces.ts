import { Node } from "react"

export interface DimensionType {
	width: number
	height: number
}

export interface RatioType {
	antecendent: number
	consequent: number
}

export interface TileType {
	dimensions: DimensionType
	ratio: RatioType | void
	item: Node
}

export interface LaneType {
	length: number
	width: number
	items: Node[]
}
