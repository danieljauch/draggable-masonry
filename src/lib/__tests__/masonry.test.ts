import { Orientation } from "../enums"
import * as Masonry from "../masonry"

describe("layout()", () => {
	it("recognizes a landscape orientation", () => {
		expect(Masonry.layout({ width: 2, height: 1 })).toBe(Orientation.LANDSCAPE)
	})

	it("recognizes a portrait orientation", () => {
		expect(Masonry.layout({ width: 1, height: 2 })).toBe(Orientation.PORTRAIT)
	})
})

describe.skip("suggestedColumns()", () => {
	it("suggests an 8 column layout", () => {
		// change window width to 1440
		expect(Masonry.suggestedColumns()).toBe(8)
	})

	it("suggests an 6 column layout", () => {
		// change window width to 1080
		expect(Masonry.suggestedColumns()).toBe(6)
	})

	it("suggests an 4 column layout", () => {
		// change window width to 960
		expect(Masonry.suggestedColumns()).toBe(4)
	})

	it("suggests an 3 column layout", () => {
		// change window width to 768
		expect(Masonry.suggestedColumns()).toBe(3)
	})

	it("suggests an 2 column layout", () => {
		// change window width to 0
		expect(Masonry.suggestedColumns()).toBe(2)
	})
})

describe("columnWidth()", () => {
	beforeAll(() => {
		const { width } = Dimensions.get("window")
	})

	it("returns window width if columns are less than 2", () => {
		expect(Masonry.columnWidth(0)).toBe(width)
		expect(Masonry.columnWidth(1)).toBe(width)
	})

	it("returns window width divided by columns when there is no gutter width", () => {
		expect(Masonry.columnWidth(2)).toBe(width / 2)
		expect(Masonry.columnWidth(3)).toBe(width / 3)
		expect(Masonry.columnWidth(4)).toBe(width / 4)
	})

	it("accounts for gutter width", () => {
		expect(Masonry.columnWidth(2, 10)).toBe((width - 10) / 2)
	})
})

describe("tileDimensionRatio()", () => {
	it("has the right ratio for a landscape tile", () => {
		expect(
			Masonry.tileDimensionRatio({
				width: 100,
				height: 50
			})
		).toEqual({
			antecedent: 2,
			consequent: 1
		})

		expect(
			Masonry.tileDimensionRatio({
				width: 11,
				height: 10
			})
		).toEqual({
			antecedent: 1.1,
			consequent: 1
		})
	})

	it("has the right ratio for a portrait tile", () => {
		expect(
			Masonry.tileDimensionRatio({
				width: 100,
				height: 50
			})
		).toEqual({
			antecedent: 1,
			consequent: 2
		})

		expect(
			Masonry.tileDimensionRatio({
				width: 10,
				height: 11
			})
		).toEqual({
			antecedent: 1,
			consequent: 1.1
		})
	})
})

describe("setDimensionsByRatio()", () => {
	it("always sets the width to the provided width", () => {
		const widthParam = 100

		expect(
			Masonry.setDimensionsByRatio(
				{
					antecendent: 1,
					consequent: 2
				},
				widthParam
			).width
		).toBe(widthParam)
		expect(
			Masonry.setDimensionsByRatio(
				{
					antecendent: 2,
					consequent: 1
				},
				widthParam
			).width
		).toBe(widthParam)
	})

	it("sets the right dimensions for a landscape tile", () => {
		const widthParam = 100
		const { height } = Masonry.setDimensionsByRatio(
			{
				antecendent: 2,
				consequent: 1
			},
			widthParam
		)

		expect(height).toBe(50)
	})

	it("sets the right dimensions for a portrait tile", () => {
		const widthParam = 100
		const { height } = Masonry.setDimensionsByRatio(
			{
				antecendent: 1,
				consequent: 2
			},
			widthParam
		)

		expect(height).toBe(200)
	})
})

describe("shortestLane()", () => {
	it("always finds the shortest lane", () => {
		const lanes = [ { length: 4 }, { length: 3 }, { length: 2 }, { length: 1 } ]

		expect(Masonry.shortestLane(lanes)).toEqual(lanes[3])
	})

	it("returns the first lane when all are equal", () => {
		const lanes = [ { length: 0 }, { length: 0 }, { length: 0 }, { length: 0 } ]

		expect(Masonry.shortestLane(lanes)).toEqual(lanes[0])
	})
})
