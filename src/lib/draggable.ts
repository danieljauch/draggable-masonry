import { StyleSheet } from "react-native"

export const dragStyles = StyleSheet.create({
	inactive: {
		opacity: 1,
		transform: "scale(1)"
	},
	active: {
		opacity: 0.2,
		transform: "scale(.8)"
	}
})

export function onLongPress() {}
