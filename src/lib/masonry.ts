import { Dimensions } from "react-native"

import { Orientation } from "./enums"
import { DimensionType, LaneType, TileType, RatioType } from "./interfaces"

export function layout({ width, height }: DimensionType) {
	if (width >= height) return Orientation.LANDSCAPE
	return Orientation.PORTRAIT
}

export function suggestedColumns(): number {
	const { width } = Dimensions.get("window")

	if (width >= 1440) return 8
	if (width >= 1080) return 6
	if (width >= 960) return 4
	if (width >= 768) return 3
	return 2
}

export function columnWidth(columns: number, gutterWidth: number = 0): number {
	const { width } = Dimensions.get("window")

	if (columns < 2) return width
	return (width - gutterWidth * (columns - 1)) / columns
}

export function tileDimensionRatio({ width, height }: DimensionType): RatioType {
	if (width >= height) {
		return {
			antecendent: width / height,
			consequent: 1
		}
	}
	return {
		antecendent: 1,
		consequent: height / width
	}
}

export function setDimensionsByRatio({ antecendent, consequent }: RatioType, width: number): DimensionType {
	return {
		width,
		height: antecendent === 1 ? consequent * width : width / antecendent * consequent
	}
}

export function shortestLane(lanes: LaneType[]): LaneType {
	return lanes.reduce((acc: LaneType, currentLane: LaneType) => (currentLane.length < acc.length ? currentLane : acc))
}

export function buildLanes(columnCount: number, tiles: TileType[], gutterWidth: number = 0): LaneType[] {
	let lanes = []
	let shortLane
	const width = columnWidth(columnCount, gutterWidth)

	for (let i = 0; i < columnCount; i++) {
		lanes.push({
			width,
			length: 0,
			tiles: []
		})
	}

	tiles.reduce((tile: TileType) => {
		shortLane = lanes.indexOf(shortestLane(lanes))
		ratio = tileDimensionRatio(tile)
		dimensions = setDimensionsByRatio(tile.dimensions)

		lanes[shortLane].length += dimensions.height
		lanes[shortLane].tiles.push({
			dimensions,
			ratio,
			item: tile.item
		})
	})

	return lanes
}
