import React, { Component } from "react"
import { ViewStyle } from "react-native"

import { Lane, Masonry, Tile } from "./src/component"
import { TileType } from "./src/lib/interfaces"

interface Props {
	lanes: number
	stylesheets: {
		containerStyle: ViewStyle
		laneStyle: ViewStyle
	}
	tiles: Array<TileType>
}

export default function App({ lanes, stylesheets, tiles }: Props) {
	return <Masonry lanes={lanes} tiles={tiles} stylesheets={stylesheets} />
}
